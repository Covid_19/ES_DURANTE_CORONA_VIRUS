//
//  Lista_Funzioni.h
//  ES DURANTE CORONA VIRUS
//
//  Created by Dario on 03/03/2020.
//  Copyright © 2020 Dario. All rights reserved.
//

#ifndef Lista_Funzioni_h
#define Lista_Funzioni_h

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <list>
#include <stack>

using namespace std;

//*************Funzione che crea l'array*****************************
//parametri passati per riferimento,crea array che dato una stringa con all'interno un espressione matematica, restituisce i numeri contenuti all'interno dell'espressione tramite l'array di interi
void Crea_Array(char s[],int numeri[],int &k){
    char s1[25]="";
    int i=0;
    int j=0;
    
    for(k=0;k<100;k++){
        numeri[k]=-1;
    }
    k=0;
    while(s[i]!='\0'){
        j=0;
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                numeri[k]=atoi(s1); // Trasformazione delle stringa Numerica in Valore Numerico Intero
                k++;                  // Contatore delle Stringhe Numeriche estratte
            }
        }
        i++;
    }
}
//*************Funzione che stampa l'array*****************************
//parametri passati per valore per far stampare l'array di interi

void Stampa_Array(int k, int numeri[]){
    for(int i=0; i<k; i++){
        
        cout<<numeri[i]<<endl;
    }
}
//*************Funzione stampa il vector*****************************

void Stampa_Vector(vector<int> numeri){
    for(int i=0; i< numeri.size(); i++){
        
        cout<<numeri[i]<<endl;
    }
}
//*************Funzione che crea il vector*****************************


void Crea_Vector(char s[], vector<int> &numeri){
    char s1[25]="";
    int i=0;
    int j=0;
    
    while(s[i]!='\0'){
        j=0;
        
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
    
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                numeri.push_back(atoi(s1)); // Trasformazione delle stringa Numerica in Valore Numerico Intero
            }
        }
        i++;
    }
}
//*******Funzione che crea la lista per mettere l'array********************
//Funzione con parametri passati per riferimento che estrae i valori dell'espressione e l'inserisce all'interno di una lista creata
struct nodo{
    int dato;
    struct nodo* succ;
};

void Crea_Lista( struct nodo*  &head, char s[]){
    char s1[25]="";
    int i=0;
    int j=0;
    struct nodo *n;
    struct nodo *scorre;
    
    while(s[i]!='\0'){
        j=0;
        
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
    
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                n=new struct nodo;
                n->dato=atoi(s1);
                if(head==NULL){
                    n->succ=head;
                    head=n;
                    
                }else{
                    scorre=head;
                    while(scorre->succ!=NULL){
                        scorre=scorre->succ;
                    }
                    n->succ=scorre->succ;
                    scorre->succ=n;
                }
            }
        }
        i++;
    }
}
//***********************Funzione che stampa la lista********************
//Funzione con parametri passati per valore, che stampa la lista
void Stampa_Lista( struct nodo* head){
    struct nodo* scorre;
    scorre=head;
    while (scorre!=NULL){
        cout<<scorre->dato<<endl;
        scorre=scorre->succ;
    }
}
//***********************Funzione che stampa la lista********************
void Crea_Classe_Lista(list <int> &valori, char s[]){
    char s1[25]="";
    int i=0;
    int j=0;
    
    while(s[i]!='\0'){
        j=0;
        
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
    
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                valori.push_back(atoi(s1)); // Trasformazione delle stringa Numerica in Valore Numerico Intero
            }
        }
        i++;
    }
}
//***********************Funzione che stampa la lista********************
//Funzione con parametri passati per valore, che stampa la classe lista
void Stampa_Classe_Lista(list <int> valori){
    list <int> :: iterator i;
    for(i= valori.begin(); i!=valori.end(); i++){
        cout<<*i<<endl;
    }
}

//******************Funzione che crea la classe stack********************
//Funzione che crea una classe stack con parametri passati per riferimento
void Crea_Classe_Stack(stack <int> &pila, char s[]){
    char s1[25]="";
    int i=0;
    int j=0;
    
    while(s[i]!='\0'){
        j=0;
        
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
    
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                pila.push(atoi(s1)); // Trasformazione delle stringa Numerica in Valore Numerico Intero
            }
        }
        i++;
    }
}
//******************Funzione che stampa la classe stack********************
//Funzione che stampa la classe stack con parametri passati per valore
void Stampa_Classe_Stack(stack <int> pila){
    while (!pila.empty()){
        cout<<pila.top()<<endl;
        pila.pop();
    }
}
//******************Funzioni per albero********************

//struct per creare le radici dell'albero
struct albero
{
    int valore;
    struct albero* Sinistro;
    struct albero* Destro;
};
//Crea la Root
struct albero* Crea_Root(int n) {
    struct albero* nuovo=new struct albero;
    nuovo->valore = n;
    nuovo->Sinistro = NULL;
    nuovo->Destro = NULL;
    return nuovo;
}


//Funzione che inserisce le radici in maniera ordinata,con parametri passati per valore
struct albero* Insert_Root(struct albero* root, int n)
{
    if (root == NULL) return Crea_Root(n);
    if (n < root->valore)
        root->Sinistro  = Insert_Root(root->Sinistro, n);
    else if (n > root->valore)
        root->Destro = Insert_Root(root->Destro, n);

    return root;
}


//Funzione che stampa l'albero, prendendo i numeri dall'espressione, e li stampa inorder "srd"
void Stampa_Inorder(struct albero* root) {
    if(root == NULL) return;
    Stampa_Inorder(root->Sinistro);
    cout<<root->valore<<endl;
    Stampa_Inorder(root->Destro);
}
//Funzione che crea l'albero con i valori dell'espressione
void Crea_Albero(struct albero* &root,char s[]){
    char s1[25]="";
    int i=0;
    int j=0;
    
    while(s[i]!='\0'){
        j=0;
        
        //QUESTO WHILE CICLA FINCHE' IN POSIZIONE i  C'E' UN NUMERO
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
    
            //! è NO NON E' UGUALE
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                root=Insert_Root(root, atoi(s1));
                
            }
        }
        i++;
    }
}
#endif /* Lista_Funzioni_h */
