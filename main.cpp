#include "Lista_Funzioni.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <list>
#include <stack>

using namespace std;
 
int main()
{
    vector<int> num;
    list <int> valori;
    stack <int> pila;
    int k;
    int numeri[100];
    char s[255];
    struct nodo* head=NULL;
    struct albero* root=NULL;
    cout<<"inserisci la tua espressione"<<endl;
    cin>>s;
    cout<<"la tua espressione é: "<<s<<endl<<endl;
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    
    Crea_Array(s, numeri, k);
    cout<<"il tuo array di interi è :"<<endl;
    Stampa_Array(k,numeri);
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    
    Crea_Vector(s, num);
    cout<<"Il tuo vector è :"<<endl;
    Stampa_Vector(num);
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    
    Crea_Lista(head, s);
    cout<<"La tua lista è :"<<endl;
    Stampa_Lista(head);
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    
    Crea_Classe_Lista(valori, s);
    cout<<"La tua classe lista è :"<<endl;
    Stampa_Classe_Lista(valori);
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    
    Crea_Classe_Stack(pila, s);
    cout<<"La tua classe stack è :"<<endl;
    Stampa_Classe_Stack(pila);
    
    cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"<<endl;
    Crea_Albero(root, s);
    cout<<"Il tuo albero inorder è :"<<endl;
    Stampa_Inorder(root);
    
}
